# docker build 
# --build-arg PORT=????
# --build-arg PG_HOST=<address>
# --build-arg PG_PORT=????
# --build-arg PG_USERNAME=<username>
# --build-arg PG_PASSWORD=<password>
# --build-arg PG_DATABASE=<database>
# --build-arg SECRET=???
# -t palikkakammo-app .
#
# docker run -d -p ????:???? palikkakammo-app

FROM node:21-alpine AS builder

WORKDIR /frontend

COPY ./palikkakammo/frontend/*.json ./
COPY ./palikkakammo/frontend/index.html ./
COPY ./palikkakammo/frontend/vite.config.ts ./
COPY ./palikkakammo/frontend/src ./src

RUN npm ci
RUN npm run build

WORKDIR /backend

COPY ./palikkakammo/backend/src ./
COPY ./palikkakammo/backend/*.json ./

RUN npm ci
RUN npm run build

FROM node:21-alpine AS final

WORKDIR /app

ARG PORT
ARG PG_HOST
ARG PG_PORT
ARG PG_USERNAME
ARG PG_PASSWORD
ARG PG_DATABASE
# ARG PG_SSL
ARG SECRET
# ARG PASSKEY

ENV PORT=${PORT}
ENV PG_HOST=${PG_HOST}
ENV PG_PORT=${PG_PORT}
ENV PG_USERNAME=${PG_USERNAME}
ENV PG_PASSWORD=${PG_PASSWORD}
ENV PG_DATABASE=${PG_DATABASE}
# ENV PG_SSL={PG_SSL}
ENV SECRET={SECRET}
# ENV PASSKEY={PASSKEY}

COPY --from=builder ./backend/package*.json ./
COPY --from=builder ./backend/dist ./dist
COPY --from=builder ./frontend/dist ./dist/client
RUN npm ci --omit=dev

EXPOSE ${PORT}
CMD ["npm", "start"]