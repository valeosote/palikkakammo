import { Request, Response, NextFunction } from "express";
import jwt, { JwtPayload } from "jsonwebtoken";
import "dotenv/config";

export interface CustomRequest extends Request {
  userId?: number | JwtPayload;
}

export const authenticate = (
  req: CustomRequest,
  res: Response,
  next: NextFunction
) => {
  const auth = req.get("Authorization");
  if (!auth?.startsWith("Bearer ")) {
    console.log("error in auth.header");
    return res.status(401).send("Invalid token");
  }
  const token = auth.substring(7);
  const secret = process.env.SECRET;
  if (secret === undefined) {
    return res.sendStatus(401);
  }

  try {
    const decodedToken = jwt.verify(token, secret);
    if (typeof decodedToken === "string")
      return console.log("Wrong type of token");
    req.body.user = decodedToken;
    req.userId = decodedToken.userId;
    next();
  } catch (error) {
    return res.status(401).send({ error: "Invalid token" });
  }
};
