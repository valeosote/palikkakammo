import { executeQuery } from "./db";

const getPersons = async () => {
  const query = "SELECT * FROM mlq.person;";
  const result = await executeQuery(query);
  return result.rows;
};

const getPersonID = async (pname: string) => {
  const query = "SELECT idperson FROM mlq.person WHERE pname = $1;";
  const parameters = [pname];
  const result = await executeQuery(query, parameters);
  if (result && result.rows && result.rows.length > 0) {
    return result.rows[0].idperson;
  } else {
    const query2 =
      "INSERT INTO mlq.person (pname) VALUES ($1) RETURNING idperson;";
    const parameters2 = [pname];
    const result2 = await executeQuery(query2, parameters2);
    if (result2.rows && result2.rows && result2.rows.length > 0) {
      const newID = result2.rows[0].idperson;
      const tname = `${pname}_alone`;
      const query3 =
        "INSERT INTO mlq.team (tname) VALUES ($1) RETURNING idteam;";
      const parameters3 = [tname];
      const result3 = await executeQuery(query3, parameters3);
      if (result3.rows && result3.rows && result3.rows.length > 0) {
        const newTeamID = result3.rows[0].idteam;
        const query4 =
          "INSERT INTO mlq.teammember (idperson, idteam) VALUES ($1, $2) RETURNING idteammember;";
        const parameters4 = [newID, newTeamID];
        const result4 = await executeQuery(query4, parameters4);
        if (result4.rows && result4.rows && result4.rows.length > 0) {
          const newTeammemberID = result4.rows[0].idteammember;
          const query5 =
            "INSERT INTO mlq.throworder (idteam) VALUES ($1) RETURNING idthroworder;";
          const parameters5 = [newTeamID];
          const result5 = await executeQuery(query5, parameters5);
          if (result5.rows && result5.rows && result5.rows.length > 0) {
            const newThrowOrderID = result5.rows[0].idthroworder;
            const query6 =
              "INSERT INTO mlq.thrower (idthroworder, idteammember, teammemberordinal) VALUES ($1, $2, $3) RETURNING idthrower;";
            const parameters6 = [newThrowOrderID, newTeammemberID, 0];
            const result6 = await executeQuery(query6, parameters6);
            if (result6.rows && result6.rows && result6.rows.length > 0) {
              const newThrowerID = result6.rows[0].idthrower;
              const query7 =
                "UPDATE mlq.thrower SET nextthrower = $1 WHERE idthrower = $2;";
              const parameters7 = [newThrowerID, newThrowerID];
              const result7 = await executeQuery(query7, parameters7);
              if (result7.rows) {
                return newID;
              } else {
                console.error("Couldn't adjust the thrower in the database.");
                return newID;
              }
            } else {
              console.error("Couldn't create a new thrower into the database.");
              return newID;
            }
          } else {
            console.error(
              "Couldn't create a new throw order into the database."
            );
            return newID;
          }
        } else {
          console.error(
            "Couldn't connect a new teammemeber into the database."
          );
          return newID;
        }
      } else {
        console.error("Couldn't enter a new team into the database.");
        return newID;
      }
    } else {
      console.error("Couldn't enter a new player into the database.");
      return -1;
    }
  }
};

const createGameEntry = async (gameresult: string, comment: string) => {
  const query =
    "INSERT INTO mlq.game (gameresult, gamecomment) VALUES ($1, $2) RETURNING idgame;";
  const parameters = [gameresult, comment];
  const result = await executeQuery(query, parameters);
  if (result.rows && result.rows && result.rows.length > 0) {
    return result.rows[0].idgame;
  } else {
    console.error("Couldn't enter a new game into the database.");
    return -1;
  }
};

const createParticipant = async (
  playerID: number,
  gameID: number,
  pord: number,
  scoreTotal: number,
  rank: number
) => {
  const query =
    "INSERT INTO mlq.participant (idteam, idgame, participantordinal, totalpoints, ranking, idthroworder) VALUES ($1, $2, $3, $4, $5, $6) RETURNING idparticipant;";
  const parameters = [playerID, gameID, pord, scoreTotal, rank, playerID];
  const result = await executeQuery(query, parameters);
  if (result.rows && result.rows && result.rows.length > 0) {
    return result.rows[0].idparticipant;
  } else {
    console.error("Couldn't enter a new participant into the database.");
    return -1;
  }
};

const connectGameToWinningParticipant = async (
  gameWinningParticipantID: number,
  gameID: number
) => {
  const query = "UPDATE mlq.game SET gamewinner = $1 WHERE idgame = $2;";
  const parameters = [gameWinningParticipantID, gameID];
  const result = await executeQuery(query, parameters);
  if (!result.rows) {
    console.error("Couldn't connect a game to the winning participant.");
  }
};

const createTurn = async (
  orderInGame: number,
  gameID: number,
  participantID: number,
  personID: number,
  turnPoints: number,
  cumulaPoints: number,
  knockedOver: number,
  specialRule: number
) => {
  const query =
    "INSERT INTO mlq.turn (orderingame, idgame, idparticipant, idperson, turnpoints, cumulativepoints, knockedover, specialrule) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING idturn;";
  const parameters = [
    orderInGame,
    gameID,
    participantID,
    personID,
    turnPoints,
    cumulaPoints,
    knockedOver,
    specialRule,
  ];
  const result = await executeQuery(query, parameters);
  if (!(result.rows && result.rows && result.rows.length > 0)) {
    console.error("Couldn't enter a new turn into the database.");
  }
};

const deleteGame = async (gameID: number) => {
  const query = "DELETE FROM mlq.game WHERE idgame = $1;";
  const parameters = [gameID];
  const result = await executeQuery(query, parameters);
  if (result.rowCount && result.rowCount > 0) {
    console.log("Game deleted from database.");
  }
};

const getGamesTableGames = async () => {
  const query = "SELECT * FROM mlq.game;";
  const result = await executeQuery(query);
  return result.rows;
};

const getParticipants = async (gameID: number) => {
  const query = "SELECT * FROM mlq.participant WHERE idgame = $1;";
  const parameters = [gameID];
  const result = await executeQuery(query, parameters);
  return result.rows;
};

const getTurns= async (participantID: number) => {
  const query = 
  "SELECT t.*, p.pname FROM mlq.turn AS t JOIN mlq.person AS p ON t.idperson = p.idperson WHERE t.idparticipant = $1;";
  const parameters = [participantID];
  const result = await executeQuery(query, parameters);
  return result.rows;
};

const getUser = async (username: string) => {
  const query = "SELECT * FROM mlq.users WHERE username = $1;";
  const parameters = [username];
  const result = await executeQuery(query, parameters);
  if (result && result.rows && result.rows.length > 0) {
    return result.rows[0];
  } else {
    return null;
  }
};

const createUser = async (username: string, passwordHash: string) => {
  const query =
    "INSERT INTO mlq.users (username, passwordhash) VALUES ($1, $2) RETURNING iduser;";
  const parameters = [username, passwordHash];
  const result = await executeQuery(query, parameters);
  if (result.rows && result.rows.length > 0) {
    const newIdUser: string = result.rows[0].iduser;
    return newIdUser;
  } else {
    console.error("No rows returned after user creation.");
    return null;
  }
};

export {
  getPersonID,
  getPersons,
  createGameEntry,
  createParticipant,
  connectGameToWinningParticipant,
  createTurn,
  deleteGame,
  getGamesTableGames,
  getParticipants,
  getTurns,
  getUser,
  createUser,
};
