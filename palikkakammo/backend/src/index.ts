import express from "express";
import "dotenv/config";
import router from "./routes";

const server = express();
server.use(express.json());
server.use("/", express.static("./dist/client"));
server.use("/palikkakammo", router);

const PORT = process.env.PORT || 3001;

server.listen(PORT, () => {
  console.log(`Express is listening to port ${PORT}.`);
});

export default server;