import express, { Request, Response } from "express";
import argon2 from "argon2";
import jwt from "jsonwebtoken";
import {
  getPersons,
  getPersonID,
  createGameEntry,
  createParticipant,
  connectGameToWinningParticipant,
  createTurn,
  deleteGame,
  getGamesTableGames,
  getParticipants,
  getTurns,
  createUser,
  getUser,
} from "./dao";
import { CustomRequest, authenticate } from "./middleware";

const router = express.Router();

router.get("/", async (_req: Request, res: Response) => {
  const result = await getPersons();
  res.send(result);
});

//Register a new user
router.post("/register", async (req: Request, res: Response) => {
  const { username, password } = req.body;

  if (!username || !password) {
    res.status(400).send("Please provide a username and password.");
  }

  const findUser = await getUser(username);
  if (findUser !== null) {
    res.status(400).send("Invalid name, please use another.");
  } else {
    const hashedPassword = await argon2.hash(password);
    const userId = await createUser(username, hashedPassword);

    const payload = { username, userId };
    const secret = process.env.SECRET ?? "salaisuus";
    const token = jwt.sign(payload, secret);

    res.status(201).send({ token });
  }
});

//Login a user
router.post("/login", async (req: Request, res: Response) => {
  const { username, password } = req.body;

  const user = await getUser(username);
  if (user === null) {
    res.sendStatus(401);
  }

  const hash = user.passwordhash;
  const passwordMatchesHash = await argon2.verify(hash, password);

  if (passwordMatchesHash) {
    console.log("Login done");

    const userId = user.user_id;
    const payload = { username, userId };
    const secret = process.env.SECRET ?? "salaisuus";
    const token = jwt.sign(payload, secret);

    res.status(200).send({ token });
  } else {
    res.sendStatus(401);
  }
});

// Get all games
router.get("/games", authenticate, async (req: Request, res: Response) => {
  const result = await getGamesTableGames();
  //console.log(result);
  res.status(200).json(result)
})

// Get participants of a game
router.get("/participants/:id", authenticate, async (req: Request, res: Response) => {
  const gameID = Number(req.params.id);
  if (!Number.isInteger(gameID)) {
    return res.status(400).send("Invalid game ID somehow.");
  }
  const result = await getParticipants(gameID);
  console.log(result);
  res.status(200).json(result);
})

// Get turns of a participant
router.get("/turns/:id", authenticate, async (req: Request, res: Response) => {
  const participantID = Number(req.params.id);
  if (!Number.isInteger(participantID)) {
    return res.status(400).send("Invalid participant ID somehow.");
  }
  const result = await getTurns(participantID);
  console.log(result);
  res.status(200).json(result);
})

// Post a game
router.post(
  "/games",
  authenticate,
  async (req: CustomRequest, res: Response) => {
    const gameData = req.body;
    //console.log(gameData);
    const player1: string = gameData.player1;
    const player2: string = gameData.player2;
    const p1throws: Array<number> = gameData.p1throws;
    const p2throws: Array<number> = gameData.p2throws;
    const p1scores: Array<number> = gameData.p1scores;
    const p2scores: Array<number> = gameData.p2scores;
    const winner: number = gameData.winner - 1;
    const comment: string = gameData.comment;
    const p1Rank = winner === 0 ? 0 : 1;
    const p2Rank = winner === 1 ? 0 : 1;

    const p1Total =
      winner === 1 &&
      p1throws[p1throws.length - 1] === 0 &&
      p1throws[p1throws.length - 2] === 0 &&
      p1throws[p1throws.length - 3] === 0
        ? 0
        : winner === 0
        ? 50
        : p1scores[p1throws.length - 1];

    const p2Total =
      winner === 0 &&
      p2throws.length > 2 &&
      p2throws[p2throws.length - 1] === 0 &&
      p2throws[p2throws.length - 2] === 0 &&
      p2throws[p2throws.length - 3] === 0
        ? 0
        : winner === 1
        ? 50
        : p2scores[p2throws.length - 1];

    const p1ID = await getPersonID(player1);
    const p2ID = await getPersonID(player2);

    const gameID = await createGameEntry(`${p1Total}-${p2Total}`, comment);

    const p1ParticipantID = await createParticipant(
      p1ID,
      gameID,
      0,
      p1Total,
      p1Rank
    );
    const p2ParticipantID = await createParticipant(
      p2ID,
      gameID,
      1,
      p2Total,
      p2Rank
    );

    const gameWinningParticipant =
      winner === 0 ? p1ParticipantID : p2ParticipantID;
    await connectGameToWinningParticipant(gameWinningParticipant, gameID);

    let i = 0;
    for (let j = 0; j < p1scores.length + p2scores.length; j++) {
      const idpart = j % 2 === 0 ? p1ParticipantID : p2ParticipantID;
      const idpers = j % 2 === 0 ? p1ID : p2ID;
      const tnpnts = j % 2 === 0 ? p1throws[i] : p2throws[i];
      const cumula = j % 2 === 0 ? p1scores[i] : p2scores[i];
      let spr = 1000;
      if (j > 3) {
        if (j % 2 === 0) {
          if (
            p1throws[i] === 0 &&
            p1throws[i - 1] === 0 &&
            p1throws[i - 2] === 0
          ) {
            spr = 0;
          } else if (p1scores[i] < p1scores[i - 1]) {
            spr = 25;
          }
        } else if (
          p2throws[i] === 0 &&
          p2throws[i - 1] === 0 &&
          p2throws[i - 2] === 0
        ) {
          spr = 0;
        } else if (p2scores[i] < p2scores[i - 1]) {
          spr = 25;
        }
      }
      await createTurn(j, gameID, idpart, idpers, tnpnts, cumula, -1, spr);
      i += j % 2;
    }

    res.status(200).json({ message: "Game posted.", gameId: gameID });
  }
);

// Delete a game
router.delete(
  "/games/:id",
  authenticate,
  async (req: Request, res: Response) => {
    const gameID = Number(req.params.id);
    if (!Number.isInteger(gameID)) {
      return res.status(400).send("Invalid game ID somehow.");
    }
    const response = await deleteGame(gameID);
    res.status(204).send();
  }
);

export default router;
