import { Dispatch, SetStateAction, useState } from "react";
import { NäytäState } from "./Navigation";

interface FormLoginProps {
  setGlobalUsername: Dispatch<SetStateAction<string>>;
  setToken: Dispatch<SetStateAction<string>>;
  setNäytä: (näitkö: NäytäState) => void;
  setSlider: (n: number) => void;
}

export default function FormLogin({
  setGlobalUsername,
  setToken,
  setNäytä,
  setSlider,
}: FormLoginProps) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const isSubmitDisabled = !(
    username.length >= 2 &&
    username.length <= 200 &&
    password.length >= 6 &&
    password.length <= 200
  );

  const handleLoginSubmit = async (e: { preventDefault: () => void }) => {
    e.preventDefault();
    //console.log("Kirjaudutaan: ", username, password);
    const response = await fetch("/palikkakammo/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ username, password }),
    });
    if (response.ok) {
      const responseData = await response.json();
      const token = responseData.token;
      setGlobalUsername(username);
      setToken(token);
      setNäytä({
        rekisteröidytään: false,
        kirjaudutaan: false,
        liity: false,
        kirjaudu: false,
        syötä: true,
        selaa: true,
        poistu: true,
      });
      setSlider(1);
    } else {
      console.error("Login failed:", response.status, response.statusText);
    }
  };
  return (
    <div className="container-form">
      <h2>Kirjaudu sisään</h2>
      <div className="container-form-login">
        <div className="row">
          <div className="left">Nimi:</div>
          <div className="right">
            <input
              type="text"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
        </div>
        <div className="row">
          <div className="left">Salasana:</div>
          <div className="right">
            <input
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
        </div>
        <div className="buttonrow">
          <button onClick={handleLoginSubmit} disabled={isSubmitDisabled}>
            Kirjaudu
          </button>
        </div>
      </div>
    </div>
  );
}
