import { useState } from "react";
import { erä } from "./App";
import Scorecard from "./Scorecard";

interface ScoreInputProps {
  submitter: (erä: erä) => void;
}

function ScoreInput({ submitter }: ScoreInputProps) {
  const firstGame: erä = {
    player1: "Pelaaja 1",
    player2: "Pelaaja 2",
    p1throws: [],
    p2throws: [],
    p1scores: [],
    p2scores: [],
    id: -1,
    comment: "",
    winner: 0,
  };
  const [newGame, setNewGame] = useState<erä>(firstGame);
  const [gameIsComplete, setGameIsComplete] = useState(false);

  const champSum = (throwvalue: number, score: number) => {
    if (throwvalue + score > 50) {
      return 25;
    } else {
      return throwvalue + score;
    }
  };
  const checkGameEndAndReturnWinner = (checkGame: erä) => {
    const iolp1s = checkGame.p1scores.length - 1;
    const iolp2s = checkGame.p2scores.length - 1;
    if (iolp1s > 1) {
      if (checkGame.p1scores[iolp1s] === 50) {
        console.log(`${checkGame.player1} sai 50 pistettä ja voitti.`);
        return 1;
      } else if (checkGame.p2scores[iolp2s] === 50) {
        console.log(`${checkGame.player2} sai 50 pistettä ja voitti.`);
        return 2;
      } else if (
        checkGame.p1throws[iolp1s] === 0 &&
        checkGame.p1throws[iolp1s - 1] === 0 &&
        checkGame.p1throws[iolp1s - 2] === 0
      ) {
        console.log(`${checkGame.player1} heitti kolme hutia ja hävisi.`);
        return 2;
      } else if (iolp2s > 1) {
        if (
          checkGame.p2throws[iolp2s] === 0 &&
          checkGame.p2throws[iolp2s - 1] === 0 &&
          checkGame.p2throws[iolp2s - 2] === 0
        ) {
          console.log(`${checkGame.player2} heitti kolme hutia ja hävisi.`);
          return 1;
        }
      }
    }
    return 0;
  };

  const handleNumberClick = (throwvalue: number) => {
    //console.log(throwvalue);
    const p1t = newGame.p1throws;
    const p2t = newGame.p2throws;
    const p1s = newGame.p1scores;
    const p2s = newGame.p2scores;
    if (p1t.length > p2t.length) {
      p2t.push(throwvalue);
      if (p2s.length === 0) {
        p2s.push(throwvalue);
      } else {
        p2s.push(champSum(throwvalue, p2s[p2s.length - 1]));
      }
    } else if (p1t.length === p2t.length) {
      p1t.push(throwvalue);
      if (p1s.length === 0) {
        p1s.push(throwvalue);
      } else {
        p1s.push(champSum(throwvalue, p1s[p1s.length - 1]));
      }
    }
    const updateGame: erä = {
      ...newGame,
      p1throws: p1t,
      p2throws: p2t,
      p1scores: p1s,
      p2scores: p2s,
    };
    const ww = checkGameEndAndReturnWinner(updateGame);
    if (ww !== 0) {
      setGameIsComplete(true);
      updateGame.winner = ww;
      //console.log(updateGame);
    }
    setNewGame(updateGame);
  };
  const handleSClick = () => {
    //console.log("S");
    const updateGame: erä = {
      ...newGame,
      player1: newGame.player2,
      player2: newGame.player1,
      p1throws: newGame.p2throws,
      p2throws: newGame.p1throws,
      p1scores: newGame.p2scores,
      p2scores: newGame.p1scores,
    };
    if (newGame.p1throws.length > newGame.p2throws.length) {
      updateGame.p1throws.push(0);
      updateGame.p1scores.push(
        updateGame.p1scores[updateGame.p1scores.length - 1]
      );
      const ww = checkGameEndAndReturnWinner(updateGame);
      if (ww !== 0) {
        updateGame.p2throws.pop();
        updateGame.p2scores.pop();
        setGameIsComplete(true);
        updateGame.winner = ww;
      }
    }
    setNewGame(updateGame);
  };
  const handleXClick = () => {
    //console.log("X");
    const p1t = newGame.p1throws;
    const p2t = newGame.p2throws;
    const p1s = newGame.p1scores;
    const p2s = newGame.p2scores;
    if (p1t.length > p2t.length) {
      p1t.pop();
      p1s.pop();
    } else if (p1t.length === p2t.length) {
      p2t.pop();
      p2s.pop();
    }
    setNewGame({
      ...newGame,
      p1throws: p1t,
      p2throws: p2t,
      p1scores: p1s,
      p2scores: p2s,
    });
    setGameIsComplete(false);
  };

  const handleNameChange = (e: { target: { value: string } }, pid: number) => {
    const p1 = pid === 1 ? e.target.value : newGame.player1;
    const p2 = pid === 2 ? e.target.value : newGame.player2;
    setNewGame({ ...newGame, player1: p1, player2: p2 });
  };
  const handleCommentChange = (e: { target: { value: string } }) => {
    setNewGame({ ...newGame, comment: e.target.value });
  };

  const handleReset = () => {
    setNewGame({
      ...firstGame,
      player1: newGame.player1,
      player2: newGame.player2,
    });
    setGameIsComplete(false);
  };

  const handleSubmit = () => {
    submitter(newGame);
    handleReset();
  };

  return (
    <div className="input-container">
      <div className="input-options">
        <div className="input-numbers">
          {[...Array(13)].map((_, index) => (
            <button
              key={index + 1}
              onClick={() => handleNumberClick((index + 1) % 13)}
              disabled={gameIsComplete}
              title={`${
                newGame.p1scores.length > newGame.p2scores.length
                  ? newGame.player2
                  : newGame.player1
              } saa ${(index + 1) % 13} pistettä.`}
            >
              {(index + 1) % 13}
            </button>
          ))}
          <button
            key="S"
            title="Vaihda pelaajien järjestystä."
            onClick={() => handleSClick()}
            disabled={gameIsComplete}
          >
            S
          </button>
          <button
            key="X"
            title="Peru viimeisin heitto."
            onClick={() => handleXClick()}
          >
            X
          </button>
        </div>
      </div>
      <div className="input-scorecard">
        <Scorecard ownID={-1} game={newGame} />
      </div>
      <div className="input-options">
        <div className="input-names">
          <input
            className="input-name"
            type="text"
            maxLength={16}
            value={newGame.player1 === "Pelaaja 1" ? "" : newGame.player1}
            placeholder="Pelaaja 1"
            onChange={(e) => handleNameChange(e, 1)}
          />
          <br />
          <input
            className="input-name"
            type="text"
            maxLength={16}
            value={newGame.player2 === "Pelaaja 2" ? "" : newGame.player2}
            placeholder="Pelaaja 2"
            onChange={(e) => handleNameChange(e, 2)}
          />
        </div>
        <div>
          <textarea
            className="textarea-comment"
            rows={3}
            placeholder="Kommentoi erää..."
            value={newGame.comment}
            onChange={(e) => handleCommentChange(e)}
          />
        </div>
        <div className="input-options-buttons">
          <button key={"tyhjennä"} onClick={handleReset}>
            Tyhjennä
          </button>
          <button
            key={"lähetä"}
            onClick={handleSubmit}
            disabled={!gameIsComplete}
          >
            Lähetä
          </button>
        </div>
      </div>
    </div>
  );
}

export default ScoreInput;
