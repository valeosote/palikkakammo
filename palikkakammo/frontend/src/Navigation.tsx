import { Dispatch, SetStateAction, useState } from "react";
import FormRegistration from "./FormRegistration";
import FormLogin from "./FormLogin";
const logourl = "../assets/MT_palikkakammo_logo.png";

export type NäytäState = {
  rekisteröidytään: boolean;
  kirjaudutaan: boolean;
  liity: boolean;
  kirjaudu: boolean;
  syötä: boolean;
  selaa: boolean;
  poistu: boolean;
};
interface NavigationProps {
  slider: number;
  setSlider: (n: number) => void;
  setToken: Dispatch<SetStateAction<string>>;
  setTrigger: Dispatch<SetStateAction<boolean>>;
}

export default function Navigation({ slider, setSlider, setToken, setTrigger }: NavigationProps) {
  const initialNäytä: NäytäState = {
    rekisteröidytään: false,
    kirjaudutaan: false,
    liity: true,
    kirjaudu: true,
    syötä: false,
    selaa: false,
    poistu: false,
  };
  const [globalUsername, setGlobalUsername] = useState("");
  const [näytä, setNäytä] = useState(initialNäytä);

  const liity = () => {
    console.log("liity clicked", näytä.rekisteröidytään);
    setNäytä({
      ...näytä,
      rekisteröidytään: !näytä.rekisteröidytään,
      kirjaudutaan: false,
    });
  };
  const kirjaudu = () => {
    console.log("kirjaudu clicked");
    setNäytä({
      ...näytä,
      rekisteröidytään: false,
      kirjaudutaan: !näytä.kirjaudutaan,
    });
  };
  const syötä = () => {
    console.log("syötä clicked");
    setSlider(1);
  };
  const selaa = () => {
    console.log("selaa clicked");
    setSlider(2);
  };
  const poistu = () => {
    console.log("poistu clicked", globalUsername);
    setNäytä(initialNäytä);
    setSlider(0);
    setGlobalUsername("");
    setToken("");
    setTrigger(true);
  };

  return (
    <div className="topnav-container">
      <div className="topnav">
        <div className="logo">
          <img src={logourl} alt="logo" />
        </div>
        <h1 className="title">Palikkakammo</h1>
        <div className="nav-words">
          {näytä.liity ? (
            <button
              className={näytä.rekisteröidytään ? "pressed" : ""}
              onClick={liity}
            >
              Liity
            </button>
          ) : (
            ""
          )}
          {näytä.kirjaudu ? (
            <button
              className={näytä.kirjaudutaan ? "pressed" : ""}
              onClick={kirjaudu}
            >
              Kirjaudu
            </button>
          ) : (
            ""
          )}
          {näytä.syötä ? (
            <button className={slider === 1 ? "pressed" : ""} onClick={syötä}>
              Syötä
            </button>
          ) : (
            ""
          )}
          {näytä.selaa ? (
            <button className={slider === 2 ? "pressed" : ""} onClick={selaa}>
              Selaa
            </button>
          ) : (
            ""
          )}
          {näytä.poistu ? <button title={`${globalUsername}, kirjaudu ulos.`} onClick={poistu}>Poistu</button> : ""}
        </div>
      </div>
      <div className="form-housing">
        {näytä.rekisteröidytään ? (
          <FormRegistration
            setGlobalUsername={setGlobalUsername}
            setToken={setToken}
            setNäytä={setNäytä}
            setSlider={setSlider}
          />
        ) : (
          ""
        )}
        {näytä.kirjaudutaan ? (
          <FormLogin
            setGlobalUsername={setGlobalUsername}
            setToken={setToken}
            setNäytä={setNäytä}
            setSlider={setSlider}
          />
        ) : (
          ""
        )}
      </div>
    </div>
  );
}
