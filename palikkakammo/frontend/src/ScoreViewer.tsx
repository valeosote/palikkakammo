//import { useState } from "react";
import { erä } from "./App";

interface ScoreInputProps {
  games: Array<erä>;
}

function ScoreViewer({ games }: ScoreInputProps) {

    const nog = games.length; // number of games
    const gameLengths = games.map(game => game.p1scores.length+game.p2scores.length);
    const averageDuration = gameLengths.reduce((a,c) => a+c , 0) / nog;
    const firstTurnScores = games.map(game => game.p1throws[0]);
    const averageFirstTurnScore = firstTurnScores.reduce((a,c) => a+c, 0) /nog;

  return (
    <div className="input-container">
      <div className="input-options">
      </div>
      <div className="input-stats">
        <h2>Pelejä yhteensä: {nog}</h2>
        <h2>Pelien keskipituus: {averageDuration.toFixed(3)}</h2>
        <h2>Aloitusten keskikoko: {averageFirstTurnScore.toFixed(3)}</h2>
      </div>
      <div className="input-options">
        <div className="input-names">
          
        </div>
      </div>
    </div>
  );
}

export default ScoreViewer;
