import { erä } from "./App";

const apiFetchAllGames = async (token: string) => {
  const responseG = await fetch("/palikkakammo/games", {
    method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  if (!responseG.ok) {
    console.log("Not much to see here.");
    return [];
  }
  const dataG: Array<{
    idgame: number;
    gamewinner: number;
    gamecomment: string;
  }> = await responseG.json();
  const gameIDs = dataG.map((obj) => obj.idgame);
  //const gameWinners = dataG.map((obj) => obj.gamewinner);
  const gameComments = dataG.map((obj) => obj.gamecomment);
  //console.log(gameIDs, gameWinners, gameComments);

  const palautettavat: Array<erä> = [];
  for (let g = 0; g < gameIDs.length; g++) {
    const newGameID = gameIDs[g];
    const newGameComment = gameComments[g];
    const responseP = await fetch(`/palikkakammo/participants/${newGameID}`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const dataP: Array<{
      idparticipant: number;
      participantordinal: number;
      ranking: number;
    }> = await responseP.json();
    const gameParticipants = dataP.map((obj) => obj.idparticipant);
    const gameParticipantOrdinals = dataP.map((obj) => obj.participantordinal);
    const gameParticipantRanking = dataP.map((obj) => obj.ranking);
    //console.log(gameParticipants,gameParticipantOrdinals,gameParticipantRanking);

    const p1pi = gameParticipantOrdinals.indexOf(0);
    const responseTp1 = await fetch(
      `/palikkakammo/turns/${gameParticipants[p1pi]}`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    const dataTp1: Array<{
      orderingame: number;
      idperson: number;
      turnpoints: number;
      cumulativepoints: number;
      pname: string;
    }> = await responseTp1.json();
    const dataTp1S = dataTp1.sort((obj) => obj.orderingame);
    const player1 = dataTp1S[0].pname;
    const p1throws = dataTp1S.map((obj) => obj.turnpoints);
    const p1scores = dataTp1S.map((obj) => obj.cumulativepoints);

    const p2pi = gameParticipantOrdinals.indexOf(1);
    const responseTp2 = await fetch(
      `/palikkakammo/turns/${gameParticipants[p2pi]}`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    const dataTp2: Array<{
      orderingame: number;
      idperson: number;
      turnpoints: number;
      cumulativepoints: number;
      pname: string;
    }> = await responseTp2.json();
    const dataTp2S = dataTp2.sort((obj) => obj.orderingame);
    const player2 = dataTp2S[0].pname;
    const p2throws = dataTp2S.map((obj) => obj.turnpoints);
    const p2scores = dataTp2S.map((obj) => obj.cumulativepoints);
    const newGameWinner = gameParticipantRanking[p1pi] === 0 ? 0 : 1;
    const newGame: erä = {
      player1,
      player2,
      p1throws,
      p2throws,
      p1scores,
      p2scores,
      id: newGameID,
      comment: newGameComment,
      winner: newGameWinner,
    };
    palautettavat.push(newGame);
  }
  return palautettavat;
};

const apiSubmitGame = async (game: erä, token: string) => {
  const response = await fetch("/palikkakammo/games", {
    method: "POST",
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
    body: JSON.stringify(game),
  });
  if (!response.ok) {
    console.log("That is not the response I was hoping for.");
    return -1;
  }
  console.log(`I claim that it is done, master.`);
  const responseData = await response.json();
  //console.log(responseData);
  return responseData.gameId;
};

const apiDeleteGame = async (game: erä, token: string) => {
  const response = await fetch(`/palikkakammo/games/${game.id}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
    body: JSON.stringify(game),
  });
  if (response.ok) {
    console.log(`Erä poistettu tietokannasta.`);
  }
};

export { apiFetchAllGames, apiSubmitGame, apiDeleteGame };
