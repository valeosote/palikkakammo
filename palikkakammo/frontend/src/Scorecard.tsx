import { erä } from "./App";

interface ScorecardProps {
  ownID: number;
  game: erä;
  pressX?: (e: React.MouseEvent<HTMLButtonElement>, oma: number) => void;
}

function Scorecard({ ownID, game, pressX }: ScorecardProps) {
  const showAsStrings = (scores: Array<number>, throws: Array<number>) => {
    return scores.map((n, i) => (throws[i] === 0 ? "—" : n.toString()));
  };

  const col1 = showAsStrings(game.p1scores, game.p1throws);
  const col2 = showAsStrings(game.p2scores, game.p2throws);
  if (col1.length > col2.length) {
    col2.push("");
  }

  const theWinner = () => {
    if (game.winner === 1) {
      return `Voittaja: ${game.player1}`;
    } else if (game.winner === 2) {
      return `Voittaja: ${game.player2}`;
    }
    return "";
  };

  return (
    <div className="scorecard" title={theWinner()}>
      <div className="scorecard-inner">
        <div className="player-names" title={game.comment}>
          {game.player1} - {game.player2}
        </div>
        <div className="score-container">
          <div className="score-grid">
            {col1.map((score, index) => (
              <div
                key={index}
                className="score-cell"
                title={`+${game.p1throws[index]}`}
              >
                {score}
              </div>
            ))}
          </div>
          <div className="score-grid">
            {col2.map((score, index) => (
              <div
                key={index}
                className="score-cell"
                title={
                  game.p2throws.length > index ? `+${game.p2throws[index]}` : ""
                }
              >
                {score}
              </div>
            ))}
          </div>
        </div>
        <div className="xbox-container">
          {ownID !== -1 ? (
            <button
              className="xbox"
              aria-label="poista"
              onClick={(e) => pressX!(e, ownID)}
              title="Poista erä."
            >
              X
            </button>
          ) : (
            ""
          )}
        </div>
      </div>
    </div>
  );
}

export default Scorecard;
