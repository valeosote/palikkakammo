import { useEffect, useState } from "react";
import "./App.css";
import ScoreInput from "./ScoreInput";
import Scorecard from "./Scorecard";
import Navigation from "./Navigation";
import { apiFetchAllGames, apiSubmitGame, apiDeleteGame } from "./apiUtils";
import ScoreViewer from "./ScoreViewer";

export interface erä {
  player1: string;
  player2: string;
  p1throws: Array<number>;
  p2throws: Array<number>;
  p1scores: Array<number>;
  p2scores: Array<number>;
  id: number;
  comment: string;
  winner: number;
}

function App() {
  const initialGames: Array<erä> = [
    {
      player1: "Aku",
      player2: "Åke",
      p1throws: [12, 8, 10, 0, 5, 7, 2],
      p2throws: [8, 0, 10, 6, 11, 8, 7],
      p1scores: [12, 20, 30, 30, 35, 42, 44],
      p2scores: [8, 8, 18, 24, 35, 43, 50],
      id: 100,
      comment: "",
      winner: 2,
    },
    {
      player1: "Matti",
      player2: "Teppo",
      p1throws: [12, 7, 10, 0, 5, 6, 10],
      p2throws: [10, 6, 8, 11, 7, 7],
      p1scores: [12, 19, 29, 29, 34, 40, 50],
      p2scores: [10, 16, 24, 35, 42, 49],
      id: 101,
      comment: "",
      winner: 1,
    },
    {
      player1: "Liisa",
      player2: "Maria",
      p1throws: [8, 10, 7, 9, 11, 5],
      p2throws: [12, 6, 8, 0, 7, 9],
      p1scores: [8, 18, 25, 34, 45, 50],
      p2scores: [12, 18, 26, 26, 33, 42],
      id: 102,
      comment: "",
      winner: 1,
    },
    {
      player1: "Jukka",
      player2: "Mikko",
      p1throws: [11, 5, 9, 6, 0, 0, 0],
      p2throws: [7, 12, 9, 8, 0, 6],
      p1scores: [11, 16, 25, 31, 31, 31, 31],
      p2scores: [7, 19, 28, 36, 36, 42],
      id: 103,
      comment: "",
      winner: 2,
    },
    {
      player1: "Tauno",
      player2: "Miika",
      p1throws: [12, 0, 9, 0, 9, 0, 12, 2, 6],
      p2throws: [9, 0, 0, 7, 8, 0, 10, 2],
      p1scores: [12, 12, 21, 21, 30, 30, 42, 44, 50],
      p2scores: [9, 9, 9, 16, 24, 24, 34, 36],
      id: 104,
      comment: "",
      winner: 1,
    },
  ];
  // const fetchAndPushGames = async() => {
  //     const games = await apiFetchAllGames();
  //     initialGames.push(...games);
  // }
  // fetchAndPushGames();

  const [games, setGames] = useState<Array<erä>>(initialGames);
  const [slider, setSlider] = useState(0);
  const [token, setToken] = useState("");
  const [trigger, setTrigger] = useState(true);

  useEffect(() => {
    const bonusStep = async () => {
      if (slider === 2 && token !== "" && trigger === true) {
        const dbgames = await apiFetchAllGames(token);
        //console.log(dbgames);
        setGames([...games, ...dbgames]);
        setTrigger(false);
      }
    };
    bonusStep();
  }, [games, slider, token, trigger]);

  const submitNewGame = async (game: erä) => {
    console.log("Submitting a new game.", game);
    const newGameID = await apiSubmitGame(game, token);
    const newGame = { ...game, id: newGameID };
    console.log(newGame);
    setGames([...games, newGame]);
  };
  const removeAGame = async (
    e: React.MouseEvent<HTMLButtonElement>,
    ownIndex: number
  ) => {
    if (e.shiftKey) {
      console.log("Erää poistetaan.");
      await apiDeleteGame(games[ownIndex], token);
    }
    setGames(games.filter((_, i) => i !== ownIndex));
  };

  return (
    <div>
      <Navigation
        slider={slider}
        setSlider={setSlider}
        setToken={setToken}
        setTrigger={setTrigger}
      />
      {slider === 1 ? (
        <div>
          <ScoreInput submitter={submitNewGame} />
        </div>
      ) : (
        ""
      )}
      {slider === 2 ? (
        <div>
          {" "}
          <ScoreViewer games={games}/>{" "}
        </div>
      ) : (
        ""
      )}
      <div className="scorecard-container">
        {games.map((game, index) => (
          <Scorecard
            key={`Scorecard: ${game.id}`}
            ownID={index}
            pressX={removeAGame}
            game={game}
          />
        ))}
      </div>
    </div>
  );
}

export default App;
