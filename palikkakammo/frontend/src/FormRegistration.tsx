import { Dispatch, SetStateAction, useState } from "react";
import { NäytäState } from "./Navigation";

interface FormRegistrationProps {
  setGlobalUsername: Dispatch<SetStateAction<string>>;
  setToken: Dispatch<SetStateAction<string>>;
  setNäytä: (näitkö: NäytäState) => void;
  setSlider: (n: number) => void;
}

export default function FormRegistration({
  setGlobalUsername,
  setToken,
  setNäytä,
  setSlider,
}: FormRegistrationProps) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [passkey, setPasskey] = useState("");

  const isSubmitDisabled = !(
    passkey === "mölkky" &&
    username.length >= 2 &&
    username.length <= 200 &&
    password.length >= 6 &&
    password.length <= 200
  );

  const handleRegistrationSubmit = async (e: {
    preventDefault: () => void;
  }) => {
    e.preventDefault();
    //console.log('Rekisteröidään: ', username, password)
    const response = await fetch("/palikkakammo/register", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ username, password }),
    });
    if (response.ok) {
      const responseData = await response.json();
      const token = responseData.token;
      setGlobalUsername(username);
      setToken(token);
      setNäytä({
        rekisteröidytään: false,
        kirjaudutaan: false,
        liity: false,
        kirjaudu: false,
        syötä: true,
        selaa: true,
        poistu: true,
      });
      setSlider(1);
    } else {
      console.error(
        "Registration failed:",
        response.status,
        response.statusText
      );
    }
  };

  return (
    <div className="container-form">
      <h2>Liity käyttäjäksi</h2>
      <div className="container-form-registration">
        <div className="row">
          <div className="left">Avain:</div>
          <div className="right">
            <input
              type="text"
              value={passkey}
              onChange={(e) => setPasskey(e.target.value)}
            />
          </div>
        </div>
        <div className="row">
          <div className="left">Nimi:</div>
          <div className="right">
            <input
              type="text"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
        </div>
        <div className="row">
          <div className="left">Salasana:</div>
          <div className="right">
            <input
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
        </div>
        <div className="buttonrow">
          <button
            onClick={handleRegistrationSubmit}
            disabled={isSubmitDisabled}
          >
            Rekisteröidy
          </button>
        </div>
      </div>
    </div>
  );
}
