# Palikkakammo

Viikon mittainen projekti. Hyvä alustus isompaan toteutukseen.

## Toteutuneet endpointit viikon aikana

- Frontend pyörii dockerin kautta paikallisesti osoitteessa [http://localhost:3000/](http://localhost:3000/)
  josta backendiin pääsee käsiksi [http://localhost:3000/palikkakammo](http://localhost:3000/palikkakammo).

- Hakee (yhteyden varmistamista varten) tietokannassa olevien pelaajien tiedot (GET):
  `"/"`

- Rekisteröi uusi käyttäjä (POST):
  `"/register"`

- Kirjaudu sisään (POST):
  `"/login"`

- Hae kaikki erät, siis yhden taulun tiedot (GET):
  `"/games"`

- Lisää uusi erä (kaikkine tietoineen) (POST):
  `"/games"`

- Poista erä id perusteella (kaikkine tietoineen) (DELETE):
  `"/games/:id"`

- Hae erän kaikki osallistujat (GET):
  `"/participants/:id"`

- Hae erän osallistujan kaikki vuorot (GET):
  `"/turns/:id"`

## Projektin status
Viiko hurahti. Dokumentointia vielä ainakin on tarkoitus laittaa myös tänne repoon. Nyt jo projektin issuesta näkee jotain mitä tässä tuli tehtyä.
